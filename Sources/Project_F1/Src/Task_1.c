#include "Task_1.h"
#include <stm32f1xx_hal_conf.h>
#include <stm32f1xx_hal.h>

static void Task1_EntryPoint(void * parameter)
{

	uint32_t value = 1;
	
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_RESET);
	vTaskDelay(1 / portTICK_PERIOD_MS);
	
	while(1)
	{		
		HAL_GPIO_WritePin(GPIOC,GPIO_PIN_8,(value==1)?GPIO_PIN_SET: GPIO_PIN_RESET);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
		
		value ^= 1;
	}
}


void Task1_Create(void)
{	
		__GPIOC_CLK_ENABLE();
	
	GPIO_InitTypeDef gpio;
	gpio.Mode = GPIO_MODE_OUTPUT_PP;
	gpio.Speed = GPIO_SPEED_HIGH;
	gpio.Pin = GPIO_PIN_8 | GPIO_PIN_9;
	
	HAL_GPIO_Init(GPIOC, &gpio);
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
	
	//Set Pin for 1-Wire, Output - OD - PU
	gpio.Mode = GPIO_MODE_OUTPUT_OD;
	gpio.Pull = GPIO_PULLUP;
	gpio.Speed = GPIO_SPEED_HIGH;
	gpio.Pin = GPIO_PIN_5;
	HAL_GPIO_Init(GPIOC, &gpio);
	
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, GPIO_PIN_SET);
	
	xTaskCreate(
		Task1_EntryPoint,
		"Task 1",
	  TASK1_STACK_SIZE,
	  NULL,
	  TASK1_PRIORITY,
	  NULL);
}