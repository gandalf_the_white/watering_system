#ifndef TASK_1_H_
#define TASK_1_H_

#include "Common.h"


#define TASK1_STACK_SIZE ( 256 )
#define TASK1_PRIORITY   (    3 )

void Task1_Create(void);


#endif 