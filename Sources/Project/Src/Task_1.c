#include "Task_1.h"
#include <stm32f4xx_hal_conf.h>
#include <stm32f4xx_hal.h>

static void Task1_EntryPoint(void * parameter)
{

	uint32_t value = 1;
	while(1)
	{
		//portMax_DELAY liefert immer true -> unendlich warten
		
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,(value==1)?GPIO_PIN_SET: GPIO_PIN_RESET);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
		
		value ^= 1;
	}
}


void Task1_Create(void)
{	
	__GPIOD_CLK_ENABLE();
	
	GPIO_InitTypeDef gpio;
	gpio.Mode = GPIO_MODE_OUTPUT_PP;
	gpio.Pin = GPIO_PIN_12;
	gpio.Speed = GPIO_SPEED_FAST;
	
	HAL_GPIO_Init(GPIOD, &gpio);
	
	xTaskCreate(
		Task1_EntryPoint,
		"Task 1",
	  TASK1_STACK_SIZE,
	  NULL,
	  TASK1_PRIORITY,
	  NULL);
}