#ifndef TASK_ADC_H_
#define TASK_ADC_H_


#include "Common.h"


#define TASK_ADC_STACK_SIZE ( 1024 )
#define TASK_ADC_PRIORITY   (    2 )

void Task_ADC_Create(void);


#endif