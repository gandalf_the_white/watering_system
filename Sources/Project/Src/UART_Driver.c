#include "UART_Driver.h"
#include <stm32f4xx_hal.h>
#include <string.h>
#include <common.h>

#define BUFFER_LENGTH			256
#define SEMAPHORE_LENGTH 	(BUFFER_LENGTH-1)
#define BAUDRATE					19200
#define PERIPH_CLK				30000000
#define	USARTDIV					(PERIPH_CLK/(BAUDRATE*16))

char TxBuffer[BUFFER_LENGTH];
SemaphoreHandle_t xTxBufferSemaphore;
uint32_t TxBufferHead = 0;
uint32_t TxBufferTail = 0;

bool UartInit(void){
	
	__GPIOA_CLK_ENABLE();
	
	GPIO_InitTypeDef gpio;
	gpio.Alternate = GPIO_AF7_USART2;
	gpio.Mode = GPIO_MODE_AF_PP;
	gpio.Pin = GPIO_PIN_2 | GPIO_PIN_3;
	gpio.Speed = GPIO_SPEED_FAST;
	
	HAL_GPIO_Init(GPIOA, &gpio);
	
	
	__USART2_CLK_ENABLE();
	
	USART2->BRR = (USARTDIV << 4) | ((USARTDIV * 16) & 0x07);
	USART2->CR1 |= USART_CR1_UE | USART_CR1_TE | USART_CR1_RE;
	
	USART2->CR1 |= USART_CR1_TXEIE | USART_CR1_TCIE;
	NVIC_SetPriority(USART2_IRQn,1);
	
	memset(TxBuffer,0,BUFFER_LENGTH);
	
	xTxBufferSemaphore = xSemaphoreCreateCounting(BUFFER_LENGTH-1, BUFFER_LENGTH-1);
	
	if(xTxBufferSemaphore == NULL){
		return false;
	}
	TxBufferHead = 0;
	TxBufferTail = 0;
	
	return true;
}

bool UartSendString(char const * data, uint32_t len, uint32_t timeout){
	while(len > 0){
				
		if(xSemaphoreTake(xTxBufferSemaphore, timeout / portTICK_PERIOD_MS) != pdTRUE){
				return false;
		}	
		
		TxBuffer[TxBufferHead] = *data;
		data++;
		TxBufferHead = (TxBufferHead+1) % BUFFER_LENGTH;
		
		len --;			
		
		NVIC_EnableIRQ(USART2_IRQn);
	}
	
	return true;
}

void USART2_IRQHandler(void){
	
	if(USART2->SR & USART_SR_TC){
		USART2->SR &= ~(USART_SR_TC);
		NVIC_ClearPendingIRQ(USART2_IRQn);
	}
	if(USART2->SR & USART_SR_TXE){
		
		USART2->DR = TxBuffer[TxBufferTail];
		TxBufferTail = (TxBufferTail+1) % BUFFER_LENGTH;
		
		if(TxBufferTail == TxBufferHead){
			NVIC_DisableIRQ(USART2_IRQn);
		}
		
		BaseType_t xHigherPriorityTaskWokenTx = pdFALSE;
		xSemaphoreGiveFromISR(xTxBufferSemaphore, &xHigherPriorityTaskWokenTx);
		
		NVIC_ClearPendingIRQ(USART2_IRQn);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWokenTx);
	}	
	
}