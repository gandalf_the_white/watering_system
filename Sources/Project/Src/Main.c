#include <stdint.h>
#include <stm32f4xx_hal_conf.h>
#include <stm32f4xx_hal.h>
#include <core_cm4.h>
#include "Common.h"

#include "Task_1.h"
#include "Task_ADC.h"
#include "UART_Driver.h"

void SystemClock_Config(void) {
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 240;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 5;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK |
                                RCC_CLOCKTYPE_PCLK1  | RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3);
}

void HAL_MspInit(void) {
  SystemClock_Config();
  // call this method to update the value of SystemCoreClock
  SystemCoreClockUpdate();
	
  SysTick_Config(SystemCoreClock/1000); //Interrupt every 1ms
}

void SysTick_Handler(){  //  
  HAL_IncTick();
}

void vAssertCalled(char const * file, int line)
{
	int x = line;
	char const* filex = file;
	while(1);
}

void vApplicationTickHook(void)
{
	HAL_IncTick();
}

//extern uint32_t os_time;
//uint32_t HAL_GetTick(void) {
//  return os_time;
//}

int main(void){
	
	HAL_Init();
	
	//HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4 );
	
	UartInit();
	
	Task1_Create();
	Task_ADC_Create();
	
	vTaskStartScheduler();
	while(1){
		
	}
	
	return 0;
}
