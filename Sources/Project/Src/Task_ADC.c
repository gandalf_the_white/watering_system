#include "Task_ADC.h"
#include <stm32f4xx_hal_conf.h>
#include <stm32f4xx_hal.h>
#include <UART_Driver.h>


SemaphoreHandle_t xADCSemaphore;

static uint32_t adcValue = 0;
static void Task_ADC_EntryPoint(void * parameter)
{

	
	uint32_t led = 0;
	while(1)
	{
	
		ADC1->CR2 |= ADC_CR2_SWSTART;
		
		xSemaphoreTake(xADCSemaphore, 500 / portTICK_PERIOD_MS);
				
		HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,(led==1)?GPIO_PIN_SET: GPIO_PIN_RESET);
		led ^= 1;
		
		vTaskDelay(1000 / portTICK_PERIOD_MS);
		
		char test[100];
		sprintf(test,"%d\n",adcValue);
		UartSendString(test,strlen(test),1000);
	}
}


void Task_ADC_Create(void)
{	
	__GPIOD_CLK_ENABLE();
	
	GPIO_InitTypeDef gpio;
	gpio.Mode = GPIO_MODE_OUTPUT_PP;
	gpio.Pin = GPIO_PIN_13;
	gpio.Speed = GPIO_SPEED_FAST;
	
	HAL_GPIO_Init(GPIOD, &gpio);
	
	__GPIOC_CLK_ENABLE();
	GPIO_InitTypeDef adcGpio;
	adcGpio.Mode = GPIO_MODE_ANALOG;
	adcGpio.Pin = GPIO_PIN_1;
	HAL_GPIO_Init(GPIOC, &adcGpio);
	
	
	__ADC1_CLK_ENABLE();
		
	
	ADC1->CR2 |= ADC_CR2_ADON;
	ADC1->SQR3 = 11;
	ADC1->CR1 |= ADC_CR1_EOCIE;
	
	
	HAL_NVIC_EnableIRQ(ADC_IRQn);
	NVIC_SetPriority(ADC_IRQn,1);
	
	xADCSemaphore = xSemaphoreCreateCounting( 1, 0 );

	if( xADCSemaphore != NULL )
	{
			/* The semaphore was created successfully. */
	}
	
	xTaskCreate(
		Task_ADC_EntryPoint,
		"Task ADC",
	  TASK_ADC_STACK_SIZE,
	  NULL,
	  TASK_ADC_PRIORITY,
	  NULL);
}

void ADC_IRQHandler(void){
	
	if(ADC1->SR & ADC_SR_EOC){
		adcValue = ADC1->DR;
		BaseType_t xHigherPriorityTaskWokenADC = pdFALSE;
		
		xSemaphoreGiveFromISR(xADCSemaphore, &xHigherPriorityTaskWokenADC);
		
		NVIC_ClearPendingIRQ(ADC_IRQn);
		portEND_SWITCHING_ISR(xHigherPriorityTaskWokenADC);
	}	else{		
		NVIC_ClearPendingIRQ(ADC_IRQn);
	}	
}