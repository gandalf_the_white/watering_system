#ifndef UART_DRIVER_H_
#define UART_DRIVER_H_

#include <stdbool.h>
#include <stdint.h>

bool UartInit(void);
bool UartSendString(char const * data, uint32_t len, uint32_t timeout);


#endif