#include "mainwidget.h"
#include "ESP8266/esp8266.h"

#include <cassert>
#include <QDebug>
#include "HAL/tslogger.h"

MainWidget* WrapperInstance = nullptr;

MainWidget::MainWidget(QWidget *parent) : QWidget(parent){

    mMainLayout = new QHBoxLayout();
    mLeftLayout = new QVBoxLayout();
    mRightLayout = new QVBoxLayout();

    mMainLayout->addLayout(mLeftLayout);
    mMainLayout->addLayout(mRightLayout);

    //Left Layout
    mComPorts = new QComboBox();
    QStringList ports = SerialPort::GetAvailablePorts();
    for(QString port : ports){
        mComPorts->addItem(port);
    }
    mOpenCloseFlag = false;

    mLeftLayout->addWidget(mComPorts);

    mOpenClosePort = new QPushButton();
    mOpenClosePort->setText(mOpenText);
    mLeftLayout->addWidget(mOpenClosePort);

    mSentProtocol = new QTextEdit();
    mSentProtocol->setReadOnly(true);
    mLeftLayout->addWidget(mSentProtocol);

    mReceivedProtocol = new QTextEdit();
    mReceivedProtocol->setReadOnly(true);
    mLeftLayout->addWidget(mReceivedProtocol);

    //Right Layout
    mResetDevice = new QPushButton(mResetDeviceText);
    mRightLayout->addWidget(mResetDevice);

    mCheckFW = new QPushButton(mCheckFWText);
    mRightLayout->addWidget(mCheckFW);

    mListNetworks = new QPushButton(mListNetworksText);
    mRightLayout->addWidget(mListNetworks);

    mRightLayout->addStretch();

    this->setLayout(mMainLayout);

    connect(mOpenClosePort,SIGNAL(pressed()),this,SLOT(OpenClosePort()));
    connect(mResetDevice,SIGNAL(pressed()),this,SLOT(ResetDevice()));
    connect(mCheckFW,SIGNAL(pressed()),this,SLOT(CheckFW()));
    connect(mListNetworks,SIGNAL(pressed()),this,SLOT(ListNetworks()));

    WrapperInstance = this;
    //Serial port
    mSerialPort = new SerialPort();
    ESP8266_Init(MainWidget::SendByteWrapper, MainWidget::ReceiveBytesWrapper, MainWidget::LineCallbackWrapper);
}

MainWidget::~MainWidget(){
    delete mComPorts; mComPorts = nullptr;
    delete mOpenClosePort; mOpenClosePort = nullptr;

    delete mSentProtocol; mSentProtocol = nullptr;
    delete mReceivedProtocol; mReceivedProtocol = nullptr;

    delete mResetDevice; mResetDevice = nullptr;
    delete mCheckFW; mCheckFW = nullptr;
    delete mListNetworks; mListNetworks = nullptr;

    delete mLeftLayout; mLeftLayout = nullptr;
    delete mRightLayout; mRightLayout = nullptr;
    delete mMainLayout; mMainLayout = nullptr;

    delete mSerialPort; mSerialPort = nullptr;
}

bool MainWidget::SendByteWrapper(const char byte){
    assert(WrapperInstance != nullptr);
    return WrapperInstance->mSerialPort->SendByte(byte);
}

bool MainWidget::ReceiveBytesWrapper(char *bytes, uint32_t *len){
    assert(WrapperInstance != nullptr);
    uint32_t length = *len;
    bool result = WrapperInstance->mSerialPort->ReceiveBytes(bytes,length);
    *len = length;
    return result;
}

void MainWidget::LineCallbackWrapper(const char * const line){
    assert(WrapperInstance != nullptr);
    WrapperInstance->mReceivedProtocol->append(QString(line));
}

void MainWidget::OpenClosePort(){

    if(mOpenCloseFlag){
        mSerialPort->ClosePort();
        mOpenClosePort->setText(mOpenText);
        mOpenCloseFlag = false;
        mComPorts->setEnabled(true);
    }else{
        if(mSerialPort->OpenPort(mComPorts->currentText())){
            mOpenClosePort->setText(mCloseText);
            mOpenCloseFlag = true;
            mComPorts->setEnabled(false);
        }
    }
}

void MainWidget::ResetDevice(){
    ESP8266_Reset();
}

void MainWidget::CheckFW(){
    ESP8266_CheckFW();
}

void MainWidget::ListNetworks(){
    uint32_t len = ESP8266_MAX_NETWORKS;
    char list[ESP8266_MAX_NETWORKS][ESP8266_MAX_SSID_SIZE] = {0};
    ESP8266_ListNetworks(list,&len);
}
