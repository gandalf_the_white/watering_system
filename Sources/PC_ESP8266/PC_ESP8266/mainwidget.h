#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>

#include <QComboBox>
#include <QPushButton>
#include <QTextEdit>

#include <QString>
#include "HAL/serialport.h"


class MainWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainWidget(QWidget *parent = nullptr);
    ~MainWidget();

private:

    QHBoxLayout* mMainLayout;
    QVBoxLayout* mLeftLayout;
    QVBoxLayout* mRightLayout;

    //Widgets for COM port selection
    QComboBox* mComPorts;
    QPushButton* mOpenClosePort;

    //Protocol widgets
    QTextEdit* mSentProtocol;
    QTextEdit* mReceivedProtocol;

    //Widgets for ESP8266
    QPushButton* mResetDevice;
    QPushButton* mCheckFW;
    QPushButton* mListNetworks;

    //Constants
    QString const mOpenText = "Open Port";
    QString const mCloseText = "Close Port";

    QString const mResetDeviceText = "Reset Device";
    QString const mCheckFWText = "Check FW Version";
    QString const mListNetworksText = "List Networks";

    //Serial port
    SerialPort* mSerialPort;
    bool mOpenCloseFlag;

    static bool SendByteWrapper(char const byte);
    static bool ReceiveBytesWrapper(char * bytes, uint32_t * len );
    static void LineCallbackWrapper(char const * const line);

private slots:

    void OpenClosePort();
    void ResetDevice();
    void CheckFW();
    void ListNetworks();
};

#endif // MAINWIDGET_H
