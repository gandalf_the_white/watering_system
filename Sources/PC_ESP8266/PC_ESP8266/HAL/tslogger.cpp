#include "tslogger.h"
#include <QDebug>
#include <QMutexLocker>

TSLogger* TSLogger::mInstance = nullptr;
QMutex TSLogger::mMutex;

TSLogger *TSLogger::GetInstance(){
    if(mInstance == nullptr){
        mMutex.lock();
        if(mInstance) mInstance = new TSLogger();
        mMutex.unlock();
    }
    return mInstance;
}

TSLogger::TSLogger(QObject *parent) : QObject(parent){

}

void TSLogger::LogToDebug(QVariant const & data){
    QMutexLocker locker(&mDebugLock);
    qDebug() << data;
}
