#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QMutex>
#include <QMutexLocker>
#include <QThread>

#include <pthread.h>

class SerialPort : public QObject
{
    Q_OBJECT
public:
    explicit SerialPort(QObject *parent = nullptr);
    ~SerialPort();

    bool OpenPort(QString const & portname);
    void ClosePort();

    static QStringList GetAvailablePorts();

    bool SendByte(char const data);
    bool ReceiveBytes(char * data, uint32_t & len);

private:
    QSerialPort* mSerialPort;

    QByteArray mReceivingBuffer;

    //Threading
    static void* ReceivingThread(void* inst);
    pthread_t mThreadHandle;
    bool mExitThread;
};

#endif // SERIALPORT_H
