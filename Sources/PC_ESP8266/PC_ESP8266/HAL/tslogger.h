#ifndef TSLOGGER_H
#define TSLOGGER_H

#include <QObject>
#include <QMutex>
#include <QVariant>

class TSLogger : public QObject
{
    Q_OBJECT
public:
    static TSLogger* GetInstance();

    void LogToDebug(QVariant const & data);

protected:
    explicit TSLogger(QObject *parent = nullptr);

private:
    static TSLogger* mInstance;
    static QMutex mMutex;

    QMutex mDebugLock;
};

#endif // TSLOGGER_H
