#include "serialport.h"
#include <QDebug>
#include <QApplication>
#include "HAL/tslogger.h"

static QMutex BufferMutex;

SerialPort::SerialPort(QObject *parent) : QObject(parent){

    mExitThread = false;
    mSerialPort = new QSerialPort();

    mSerialPort->setBaudRate(QSerialPort::Baud115200);
    mSerialPort->setStopBits(QSerialPort::OneStop);
    mSerialPort->setDataBits(QSerialPort::Data8);
}

SerialPort::~SerialPort(){
    ClosePort();
    delete mSerialPort; mSerialPort = nullptr;
}

bool SerialPort::OpenPort(const QString &portname){
    if(portname.isEmpty()){
        return false;
    }
    mSerialPort->setPortName(portname);

    if(mSerialPort->open(QIODevice::ReadWrite)){
        pthread_create(&mThreadHandle, nullptr, SerialPort::ReceivingThread, this);
        return true;
    }else{
        return false;
    }
}

void SerialPort::ClosePort(){

    mExitThread = true;
    pthread_join(mThreadHandle, nullptr);

    mSerialPort->close();
}

QStringList SerialPort::GetAvailablePorts(){

    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();
    QStringList result;
    for(auto info : ports){
        result.append(info.portName());
    }
    return result;
}

bool SerialPort::SendByte(const char data){
    return (mSerialPort->write(&data,1) == 1);
}

bool SerialPort::ReceiveBytes(char *data, uint32_t &len){
    if(len == 0){
        //illegal value for buffer size
        return false;
    }

    QApplication::processEvents();
    QMutexLocker locker(&BufferMutex);
    uint32_t bytesToCopy = (mReceivingBuffer.size() > len)? len : mReceivingBuffer.size();
    memcpy(data,mReceivingBuffer.data(), bytesToCopy);

    mReceivingBuffer.remove(0,bytesToCopy);

    len = bytesToCopy;
    return true;
}

#include <iostream>
using namespace std;
void* SerialPort::ReceivingThread(void* inst){

    qDebug() << "Receiving Thread started";
    SerialPort* instance = static_cast<SerialPort*>(inst);

    while(!instance->mExitThread){
        if(instance->mSerialPort->waitForReadyRead(1000)){

            {
            QMutexLocker locker(&BufferMutex);
            instance->mReceivingBuffer.append(instance->mSerialPort->readAll());
            }
            QApplication::processEvents();
            //qDebug() << "Received Bytes: " << instance->mReceivingBuffer;
            //qDebug() << "Rec fin";
        }else{
            QApplication::processEvents();
            if(instance->mReceivingBuffer.size())
                qDebug() << "Rest " << instance->mReceivingBuffer.size();
        }
    }

    qDebug() << "Receiving Thread finished";
}
