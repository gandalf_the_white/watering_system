#ifndef ESP8266_H
#define ESP8266_H

#include <stdint.h>

#define QT_DEBUG

#define ESP8266_RX_BUFFER_SIZE  256
#define ESP8266_RX_MAX_LINE_LEN (32+1)
#define ESP8266_MAX_SSID_SIZE   32
#define ESP8266_MAX_NETWORKS    16

typedef enum {
    eESP8266_NFIN = 2, //No end tag received
    eESP8266_RDY = 1, //Ready tag received
    eESP8266_OK = 0, //OK tag received
    eESP8266_NOK = -1 //ERROR tag received
}TESP8266_ReadResult;

typedef bool (*TESP8266_SendByte) (char const byte);
typedef bool (*TESP8266_ReceiveBytes)(char * bytes, uint32_t * len);
typedef void (*TESP8266_LineCallback)(char const * const line);

bool ESP8266_Init(TESP8266_SendByte sendbyte, TESP8266_ReceiveBytes receiveBytes, TESP8266_LineCallback linecb);

bool ESP8266_Reset();
bool ESP8266_CheckFW();

bool ESP8266_ListNetworks(char list[][ESP8266_MAX_SSID_SIZE], uint32_t * len);
bool ESP8266_ConnectToNetwork(char const * const ssid, char const * const user, char const * const password);
bool ESP8266_DisconnectFromNetwork();

bool ESP8266_ConnectToHost(char const * const host, uint16_t const port);
bool ESP8266_DisconnectFromHost();

#endif // ESP8266_H
