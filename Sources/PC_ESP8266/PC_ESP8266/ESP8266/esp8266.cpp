#include "esp8266.h"
#include <string.h>
#include <stdio.h>

#ifdef QT_DEBUG
#include <QDebug>
#endif

//Function Prototypes
bool ESP8266_WriteLine(char const * const data);
TESP8266_ReadResult ESP8266_ReadLine();
TESP8266_ReadResult ESP8266_ReadMultipleLines();
void ESP8266_FillRxBuffer();

TESP8266_SendByte InternalSendByte = NULL;
TESP8266_ReceiveBytes InternalReceiveBytes = NULL;
TESP8266_LineCallback InternalLineCallback = NULL;

//Receiving Buffer
static char RxBuffer[ESP8266_RX_BUFFER_SIZE];
uint32_t RxHeadBuffer = 0;
uint32_t RxTailBuffer = 0;

static char RxLineBuffer[ESP8266_RX_MAX_LINE_LEN+1];

//Command Definitions
static char const * const CMD_RESET = "AT+RST";
static char const * const CMD_VERSION = "AT+GMR";
static char const * const CMD_LIST_NETWORKS = "AT+CWLAP";

static char const * const RES_OK = "OK";
static char const * const RES_NOK = "ERROR";
static char const * const RES_RDY = "ready";

bool ESP8266_Init(TESP8266_SendByte sendbyte, TESP8266_ReceiveBytes receiveBytes, TESP8266_LineCallback linecb){
    if (sendbyte == NULL || receiveBytes == NULL){
        return false;
    }
    InternalSendByte = sendbyte;
    InternalReceiveBytes = receiveBytes;

    if(linecb != NULL){
        InternalLineCallback = linecb;
    }

    memset(RxBuffer,0,ESP8266_RX_BUFFER_SIZE);
    memset(RxLineBuffer,0,ESP8266_RX_MAX_LINE_LEN);

    return true;
}

bool ESP8266_ConnectToNetwork(const char * const ssid, const char * const user, const char * const password){

}

bool ESP8266_Reset(){
    if(ESP8266_WriteLine(CMD_RESET) == false){
        return false;
    }

    TESP8266_ReadResult res;
    res = ESP8266_ReadMultipleLines();
#ifdef QT_DEBUG
    qDebug() << "Reset, res 1 = " << res;
#endif

    res = ESP8266_ReadMultipleLines();
#ifdef QT_DEBUG
    qDebug() << "Reset, res 2 = " << res;
#endif
    return true;
}

bool ESP8266_WriteLine(char const * const data){
    if(InternalSendByte == NULL){
        return false;
    }

    char const * sentByte = data;
    while(*sentByte){
        InternalSendByte(*sentByte);
        sentByte++;
    }

    InternalSendByte('\r');
    InternalSendByte('\n');
    return true;
}

TESP8266_ReadResult ESP8266_ReadLine(){

    bool CarriageReturnFlag = false;
    uint32_t lineIdx = 0;

    ESP8266_FillRxBuffer();
    //Check for line until \r\n is read or maximum length is reached
    while(lineIdx <= ESP8266_RX_MAX_LINE_LEN){
        if(RxHeadBuffer == RxTailBuffer){
            //TODO add delay
            ESP8266_FillRxBuffer();
            continue;
        }
        if(RxBuffer[RxHeadBuffer] == '\r'){
            CarriageReturnFlag = true;
        }else if(RxBuffer[RxHeadBuffer] == '\n' && CarriageReturnFlag){

            RxHeadBuffer = (RxHeadBuffer+1) % ESP8266_RX_BUFFER_SIZE;
            break;
        }else{
            CarriageReturnFlag = false;
            RxLineBuffer[lineIdx] = RxBuffer[RxHeadBuffer];

            lineIdx++;
        }

        RxHeadBuffer = (RxHeadBuffer+1) % ESP8266_RX_BUFFER_SIZE;
    }
    RxLineBuffer[lineIdx] = 0; //terminate line
#ifdef QT_DEBUG
    qDebug() << RxLineBuffer;
#endif
    if(InternalLineCallback){
        InternalLineCallback(RxLineBuffer);
    }

    if(strcmp(RxLineBuffer,RES_OK) == 0){
        return eESP8266_OK;
    }else if(strcmp(RxLineBuffer, RES_NOK) == 0){
        return eESP8266_NOK;
    }else if (strcmp(RxLineBuffer, RES_RDY) == 0) {
        return eESP8266_RDY;
    }else{
        return eESP8266_NFIN;
    }
}

TESP8266_ReadResult ESP8266_ReadMultipleLines(){


    TESP8266_ReadResult res;
    while((res = ESP8266_ReadLine()) == eESP8266_NFIN){

    }
    return res;
}

bool ESP8266_ListNetworks(char list[][ESP8266_MAX_SSID_SIZE], uint32_t *len){

    if(list == NULL || (*list) == NULL || len == NULL){
        return false;
    }
    if(*len == 0){
        return false;
    }

    if(ESP8266_WriteLine(CMD_LIST_NETWORKS) == false){
        return false;
    }

    uint32_t maxListLength = *len;
    uint32_t listIdx = 0;
    uint32_t ssidIdx = 0;

    enum TParsingState{
        eNetworkStart,
        eSsidStarted,
        eSsidEnd,
        eNetworkEnd
    };
    TParsingState parsingState = eNetworkEnd;

    TESP8266_ReadResult res;
    while((res = ESP8266_ReadLine()) == eESP8266_NFIN){

        uint32_t lineIdx = 0;
        while(RxLineBuffer[lineIdx] != 0 && listIdx < maxListLength){
            switch(parsingState){
            case eNetworkStart:
                if(RxLineBuffer[lineIdx] == '\"'){
                    parsingState = eSsidStarted;
                }
                break;
            case eSsidStarted:
                if(RxLineBuffer[lineIdx] != '\"') {
                    list[listIdx][ssidIdx++] = RxLineBuffer[lineIdx];
                }else{
#ifdef QT_DEBUG
                    qDebug() << "Netz " << listIdx << ": " << list[listIdx];
#endif
                    ssidIdx = 0;
                    listIdx++;
                    parsingState = eSsidEnd;
                }

                break;
            case eSsidEnd:
                if(RxLineBuffer[lineIdx] == ')'){
                    parsingState = eNetworkEnd;
                }
                break;
            case eNetworkEnd:
                if(RxLineBuffer[lineIdx] == '('){
                    parsingState = eNetworkStart;
                }
                break;
            }

            lineIdx++;
        }
    }
    *len = listIdx;

#ifdef QT_DEBUG
    qDebug() << "List Networks res 1 = " << res;
#endif
    return true;
}

void ESP8266_FillRxBuffer(){
    if(((RxTailBuffer+ESP8266_RX_BUFFER_SIZE)-RxHeadBuffer) == 0){
        //No free space in rx buffer
        return;
    }

    uint32_t copyBytes = 0;
    if(RxTailBuffer < RxHeadBuffer){
        copyBytes = RxHeadBuffer-RxTailBuffer;
        InternalReceiveBytes(&RxBuffer[RxTailBuffer], &copyBytes);
        RxTailBuffer = (RxTailBuffer+copyBytes) % ESP8266_RX_BUFFER_SIZE;
    }else{
        copyBytes = ESP8266_RX_BUFFER_SIZE-RxTailBuffer;
        InternalReceiveBytes(&RxBuffer[RxTailBuffer], &copyBytes);
        RxTailBuffer = (RxTailBuffer+copyBytes) % ESP8266_RX_BUFFER_SIZE;

        if(RxTailBuffer > 0){
            copyBytes = RxHeadBuffer-1;
            //qDebug() << "Fill Buffer with " << copyBytes << ", tail : " << RxTailBuffer << ", head : " << RxHeadBuffer;
            InternalReceiveBytes(&RxBuffer[RxTailBuffer], &copyBytes);
            //qDebug() << "Filled Buffer with " << copyBytes;
            RxTailBuffer = (RxTailBuffer+copyBytes) % ESP8266_RX_BUFFER_SIZE;
        }
    }
}

bool ESP8266_CheckFW(){
    if(ESP8266_WriteLine(CMD_VERSION) == false){
        return false;
    }

    TESP8266_ReadResult res;
    res = ESP8266_ReadMultipleLines();
#ifdef QT_DEBUG
    qDebug() << "Get Version, res 1 = " << res;
#endif
    return true;
}

bool ESP8266_DisconnectFromNetwork(){

}

bool ESP8266_ConnectToHost(const char * const host, const uint16_t port){

}

bool ESP8266_DisconnectFromHost(){

}
