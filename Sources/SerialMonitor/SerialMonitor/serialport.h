#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QObject>
#include <QtSerialPort/QSerialPort>

class SerialPort : public QObject
{
    Q_OBJECT
public:
    explicit SerialPort(QObject *parent = nullptr);
    ~SerialPort();

    static QStringList GetAvailablePorts();

signals:

public slots:

private:

    QSerialPort* mSerialPort;

};

#endif // SERIALPORT_H
