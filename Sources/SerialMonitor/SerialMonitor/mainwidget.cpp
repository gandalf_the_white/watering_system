#include "mainwidget.h"
#include "serialport.h"

MainWidget::MainWidget(QWidget *parent) : QWidget(parent){

    mMainLayout = new QVBoxLayout();

    mFirstPort = new QComboBox();
    mSecondPort = new QComboBox();

    mMainLayout->addWidget(mFirstPort);
    mMainLayout->addWidget(mSecondPort);

    QStringList ports = SerialPort::GetAvailablePorts();
    for(auto port : ports){
        mFirstPort->addItem(port);
        mSecondPort->addItem(port);
    }

    this->setLayout(mMainLayout);
}

MainWidget::~MainWidget(){

    delete mFirstPort;
    mFirstPort = nullptr;

    delete mSecondPort;
    mSecondPort = nullptr;

    delete mMainLayout;
    mMainLayout = nullptr;
}
