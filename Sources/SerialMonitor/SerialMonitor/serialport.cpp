#include "serialport.h"
#include <QSerialPortInfo>
#include <QDebug>

SerialPort::SerialPort(QObject *parent) : QObject(parent){
    mSerialPort = new QSerialPort();
}

SerialPort::~SerialPort(){
    delete mSerialPort;
    mSerialPort = nullptr;
}

QStringList SerialPort::GetAvailablePorts(){

    QList<QSerialPortInfo> ports = QSerialPortInfo::availablePorts();

    QStringList result;
    for(auto port : ports) {
        result << port.portName();
    }
    return result;
}
