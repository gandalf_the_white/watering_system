#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>

#include <QTextEdit>
#include <QPushButton>
#include <QComboBox>

class MainWidget : public QWidget{
    Q_OBJECT
public:
    MainWidget(QWidget* parent = 0);
    ~MainWidget();

private:

    QVBoxLayout * mMainLayout;

    QComboBox* mFirstPort;
    QComboBox* mSecondPort;
};

#endif // MAINWIDGET_H
