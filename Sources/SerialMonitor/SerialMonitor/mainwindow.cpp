#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    mMainWidget = new MainWidget(this);

    this->setCentralWidget(mMainWidget);
}


MainWindow::~MainWindow(){
    delete mMainWidget;
    mMainWidget = nullptr;
}
